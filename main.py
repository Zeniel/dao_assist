import sys
from settings import load_settings, DEAFULT_SETTING_FILE_PATH

from files import copy_settings_to_doaspec_dir, backup_daospec_settings, restore_daospec_settings, daospec_log_subdirectory

from daospec import iterate_doaspec_over_all_stars


def main(settings_file_path):
    """
    Entry point of the program.

    Parameters
    ----------
    settings_file_path : string
        Path to the setting JSON file. The default path `settings.json` is used if None.
    """

    options = load_settings(settings_file_path)
    log_subdirectory = daospec_log_subdirectory()
    backup_daospec_settings(options)
    copy_settings_to_doaspec_dir(options)
    iterate_doaspec_over_all_stars(options, log_subdirectory=log_subdirectory)
    restore_daospec_settings(options)
    print(f"\nWe are done!")


def run_with_command_line_arguments(arguments):
    """
    Run the program with command line arguments function.

    Parameters
    ----------
    arguments : list of str
        command line argument supplied.
    """

    if len(arguments) > 1:
        print("Incorrect command line argument.")
        print("Usage: ")
        print("python main.py [SETTINGS_PATH]")
        print("   SETTINGS_PATH is an optional path to the settings file,")
        print(f"   {DEAFULT_SETTING_FILE_PATH} is used if SETTINGS_PATH is not supplied.")
        return

    settings_file_path = None

    if len(arguments) == 1:
        settings_file_path = arguments[0]

    main(settings_file_path)


if __name__ == '__main__':
    run_with_command_line_arguments(sys.argv[1:])
