# DAO Assist, a Python utility for running DAOSPEC

DAO Assist is a Python utility that automates calculation of equivalent widths for selected absorption lines in stellar spectra using DAOSPEC program. 

## Why DAO Assist program was needed?

When DAOSPEC is run manually, it finds equivalent widths from a single range of wavelengths in one .fits fits. Along with the results DAOSPEC also returns a FWHM number (full width half maximum). One needs to run DAOSPEC again and supply this FWHM value as a `fw=` option. This process needs to be repeated many times until the returned FWHM numbers stop changing significantly.

These are three tasks that DAO Assist assists with:

1. It runs DAOSPEC multiple times until the difference between the returned FWHM values becomes smaller than the `fwhm_tolerance` setting specified in [settings.json](settings.json) file.

1. It runs DAOSPEC for each .fits file in directory specified in `fits_dir` setting.

1. Finally, one can specify multiple ranges of wavelengths for processing using `wavelength_ranges_angstrom` setting. This is useful if a .fits file contains spectrum from multiple CCDs (red, gree, blue), but has no signal between the ranges of the CCDs. The problem is that DAOSPEC does not work if the range includes empty signal. Therefore, normally, multiple runs would be required to process all wavelength ranges in a .fits file.


## Usage

* First, download DAO Assist source code (requires [Git](https://git-scm.com)):


```
git clone https://gitlab.com/Zeniel/dao_assist.git
cd dao_assist
```

* Next, [install DAOSPEC](https://gitlab.com/evgenyneu/2018_summer_project_logbook/tree/master/a2019/a02/a26_installing_daospec) and put the path to its directory into `daospec_dir` setting of [settings.json](settings.json) file.

* Put .FITS files that you want to process with DAOSPEC in one directory, and specify its path in `fits_dir` setting. IMPORTANT: the `fits_dir` path needs to be short. Unfortunately, DAOSPEC only accepts paths to .fits files that are no longer than 40 characters. Therefore, we recommend to use a directory with a short name located in the root of the file system (for example, `/fits`).

* Specify the directory for the DAOSPEC output in `moog_input_dir` setting. This is the directory where DAOSPEC will put files containing the measured equivalent widths.

* Update the `wavelength_ranges_angstrom` setting with the list of wavelength ranges for your .FITS files that you want to process.

* Update [laboratory.dat](laboratory.dat) with the list of absorption lines that you want DAOSPEC to measure. It is sufficient to replace first two numbers (wavelength in Angstrom and element number) in each row. WARNING make sure there are no extra spaces after the final line of the laboratory.dat file or you will break the code!!!

* Finally, run the program (requires Python):

```
python main.py
```

The program will create *.element.lines and *.fe.lines files [like these ones](test_expectations) in `moog_input_dir` directory. The last columns in these files are the equivalent width values estimated by DAOSPEC.




### Specifying a path to a settings file

By default, the program uses the `settings.json` file. Alternatively, one can supply a path to a different settings file:

```
python main.py my_custom_settings.json
```


## DAO Assist settings

To configure the program settings, change their vales in the [settings.json](settings.json) file:


* **daospec_dir**: Path to a directory that contains `daospec` executable.

* **fits_dir**: Path to a directory containing .fits files that will be processed by DAOSPEC. IMPORTANT: the path needs to be short. Unfortunately, DAOSPEC only accepts paths to .fits files that are no longer than 40 characters. Therefore, we recommend to use a directory with a short name located in the root of the file system (for example, `/fits`).

* **moog_input_dir**: The directory where DAOSPEC will put the output files containing the calculated equivalent widths.

* **daospec_log_dir**: The directory where DAOSPEC output files will be copied.

* **daospec_opt_path**: Path to the file containing default options for the DAOSPEC. The file will be copied into the `daospec.opt` file located in `daospec_dir` directory before DAOSPEC is run.

* **laboratory_dat_path**: Path to the file containing the list of absorption lines that will be measured by DAOSPEC.  The file will be copied into the `laboratory.dat` file located in `daospec_dir` directory before DAOSPEC is run.

* **wavelength_ranges_angstrom**: List of wavelength ranges that will be processed by DAOSPEC.

* **max_fwhm_iterations**: Maximum number of times DAOSPEC is run each wavelength interval.

* **fwhm_tolerance**: This value is used for comparing the FWHM (full width half maximum) numbers reported by DAOSPEC. DAOSPEC is run repeatedly until the absolute value of the difference between FWFM values from successive runs is smaller than `fwhm_tolerance`, or the number of runs exceeds `max_fwhm_iterations`.



## Run integration tests

The test runs DAO Assist program using two .fits files located in [test_fits](test_fits) directory. The output files are compared with expected output located in [test_expectations](test_expectations) directory.


1. Install the testing library

```
pip install pytest
```

2. Change `daospec_dir` in [test_settings.json](test_settings.json) to point to your daospec directory. Leave all other options unchanged.

3. Run tests

```
pytest
```



## The unlicense

This work is in [public domain](LICENSE).

