from main import main
import os
import shutil


def assert_files_equal(filename1, filename2):
    with open(filename1, 'r') as file_expect:
        text_expect = file_expect.read()

        with open(filename2, 'r') as file:
            text_actual = file.read()
            assert text_expect == text_actual


def test_main():
    basename1 = "AGBSYAZ016610"
    basename2 = "RGBSYAZ030421"

    file1_elements_elements = f"{basename1}.elements.lines"
    file1_elements_fe = f"{basename1}.fe.lines"
    file2_elements_elements = f"{basename2}.elements.lines"
    file2_elements_fe = f"{basename2}.fe.lines"

    test_data_dir = "test_output"
    if os.path.exists(test_data_dir):
        shutil.rmtree(test_data_dir)

    main("test_settings.json")

    assert(os.path.exists(test_data_dir))

    file1_elements_elements_full = os.path.join(test_data_dir, file1_elements_elements)
    file1_elements_fe_full = os.path.join(test_data_dir, file1_elements_fe)
    file2_elements_elements_full = os.path.join(test_data_dir, file2_elements_elements)
    file2_elements_fe_full = os.path.join(test_data_dir, file2_elements_fe)

    assert(os.path.exists(file1_elements_elements_full))
    assert(os.path.exists(file1_elements_fe_full))
    assert(os.path.exists(file2_elements_elements_full))
    assert(os.path.exists(file2_elements_fe_full))

    test_data_expec_dir = "test_expectations"

    # File 1
    # ------------

    file1_elements_elements_full_expect = os.path.join(
        test_data_expec_dir, file1_elements_elements)

    assert_files_equal(file1_elements_elements_full_expect, file1_elements_elements_full)

    file1_elements_fe_full_expect = os.path.join(
        test_data_expec_dir, file1_elements_fe)

    assert_files_equal(file1_elements_fe_full_expect, file1_elements_fe_full)

    # File 2
    # ------------

    file2_elements_elements_full_expect = os.path.join(
        test_data_expec_dir, file2_elements_elements)

    assert_files_equal(file2_elements_elements_full_expect, file2_elements_elements_full)

    file2_elements_fe_full_expect = os.path.join(
        test_data_expec_dir, file2_elements_fe)

    assert_files_equal(file2_elements_fe_full_expect, file2_elements_fe_full)

    shutil.rmtree(test_data_dir)