"""
Running Daospec.
"""

import subprocess
import os
from parser import extract_fwhm, extract_results

from files import files_in_dir, remove_files, daospec_working_dir, copy_daospec_output_to_log_dir

from moog import save_daospec_output_for_moog


def run_daospec(path_to_fits, wavelength_start, wavelength_end, options, fwhm=None):
    """
    Process the same .fits file with Daospec repeatedly,
    until the changes in returned FWHM value
    are smaller than `fwhm_tolerance`.

    Parameters
    ----------
    path_to_fits : str
        Path to the .fits file to be used by the Daospec.
    wavelength_start: float
        The start of the wavelength range used by Daospec.
    wavelength_end: float
        The end of the wavelength range used by Daospec.
    options: dict
        Program options.
    fwhm: float
        None if this is the first iteration. Othewise, this is 
        the FWHM value returned by this function from the previous iterations.

    Returns
    -------
    float
       FWHM value reported by Daospec.
    """

    if fwhm is None:
        fwhm_option = ""
    else:
        fwhm_option = f"\nfw={fwhm}"

    daospec_dir = options["daospec_dir"]
    doaspec_program_path = os.path.join(daospec_dir, "daospec")
    working_dir = daospec_working_dir(options)

    process = subprocess.run(
        [doaspec_program_path],
        input=f'wa=0\nsh={wavelength_start}\nlo={wavelength_end}{fwhm_option}\n\n{path_to_fits}\n',
        cwd=working_dir,
        encoding='ascii',
        capture_output=True)

    return extract_fwhm(process.stdout)


def iterate_daospec_over_fwhm(basename, fits_dir,
                              wavelength_start, wavelength_end, options):
    """
    Process the same .fits file with Daospec repeatedly,
    until the changes in returned FWHM value
    are smaller than `fwhm_tolerance`.

    Parameters
    ----------
    basename : str
        Basename of the .fits file.
    fits_dir : str
        Directory of the .fits file.
    wavelength_start: float
        The start of the wavelength range used by Daospec.
    wavelength_end: float
        The end of the wavelength range used by Daospec.
    options: dict
        Program options.
    """

    file_fits = f"{basename}.fits"
    path_to_fits = os.path.join(fits_dir, file_fits)

    fwhm_previous = None
    fwhm_tolerance = options["fwhm_tolerance"]
    max_iterations = options["max_fwhm_iterations"]

    print(f"Wavelengths: {wavelength_start} - {wavelength_end} A")

    for i in range(max_iterations):
        remove_files(fits_dir=fits_dir, basename=basename)

        fwhm = run_daospec(path_to_fits, fwhm=fwhm_previous,
                           wavelength_start=wavelength_start,
                           wavelength_end=wavelength_end,
                           options=options)

        print(f"{i + 1}. FWHM: {fwhm}")

        if fwhm_previous is not None and abs(fwhm_previous - fwhm) < fwhm_tolerance:
            break

        fwhm_previous = fwhm

    if i == max_iterations - 1:
        print(f"Reached the singularity: {i}")


def iterate_doaspec_over_wavelengths(basename, fits_dir, options, log_subdirectory):
    """
    Process all the wavelength ranges in the given fits file using Daospec.

    Parameters
    ----------
    basename : str
        Basename of the .fits file.
    fits_dir : str
        Directory of the .fits file.
    options: dict
        Program options.
    log_subdirectory : str
        Name of the directory where the DAOSPEC output will be copied.

    Returns
    -------
    list of str
       List of lines returned by Daospec.
    """

    wavelength_ranges = options["wavelength_ranges_angstrom"]
    all_lines = []

    for wavelength_range in wavelength_ranges:
        wavelength_start = wavelength_range["start"]
        wavelength_end = wavelength_range["end"]

        iterate_daospec_over_fwhm(basename, fits_dir=fits_dir,
                                  wavelength_start=wavelength_start,
                                  wavelength_end=wavelength_end,
                                  options=options)

        lines = extract_results(basename, fits_dir=fits_dir)

        copy_daospec_output_to_log_dir(
            basename=basename,
            options=options,
            log_subdirectory=log_subdirectory,
            wavelength_start=wavelength_start,
            wavelength_end=wavelength_end
        )

        all_lines += lines

    return all_lines


def iterate_doaspec_over_all_stars(options, log_subdirectory):
    """
    Uses Daospec to process all .fits located in the `fits_dir` setting.

    Parameters
    ----------
    options : dict
        Program options.
    log_subdirectory : str
        Name of the directory where the DAOSPEC output will be copied.
    """

    fits_dir = options["fits_dir"]
    basenames = files_in_dir(fits_dir, ".fits")

    # Ignore .fits files created by Daospec (ending with R.fits and C.fits)
    basenames = [basename for basename in basenames if basename[-1] != 'R' and basename[-1] != 'C']

    for basename in basenames:
        print(f"\nDaospecing: {basename}")

        lines = iterate_doaspec_over_wavelengths(
            basename,
            fits_dir=fits_dir,
            options=options,
            log_subdirectory=log_subdirectory)

        save_daospec_output_for_moog(basename=basename, lines=lines, options=options)
        remove_files(basename, fits_dir=fits_dir)