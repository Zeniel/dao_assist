"""
File operations
"""

import os
from shutil import copyfile
import datetime


# Names of the DAOSPEC option files
DAOSPEC_OPT_FILE_NAME = "daospec.opt"
LABORATORY_DAT_FILE_NAME = "laboratory.dat"

# Text added to the end of the backup .opt and .dat file names
BACKUP_SUFFIX = ".dao_assist_backup"


def daospec_working_dir(options):
    """
    Returns the path to the DAOSPEC working directory.
    This directory is `daospec_working_dir` option setting
    (only used in tests)

    If `daospec_working_dir` is not specified (which is normal),
    then `daospec_dir` is used instead.

    Parameters
    ----------
    options : dict
        Program options.

    Returns
    -------
    string
        Path to the DAOSPEC working directory.
    """

    working_dir = options["daospec_dir"]

    if "daospec_working_dir" in options:
        working_dir = options["daospec_working_dir"]

    return working_dir


def remove_files(basename, fits_dir):
    """
    Removes files produced by Daospec.

    Parameters
    ----------
    basename : str
        Basename of the .fits file.
    fits_dir : str
        Directory of the .fits file.

    """

    file_daospec = f"{basename}.daospec"
    file_c = f"{basename}C.fits"
    file_r = f"{basename}R.fits"

    path_to_daospec = os.path.join(fits_dir, file_daospec)
    path_to_r = os.path.join(fits_dir, file_r)
    path_to_c = os.path.join(fits_dir, file_c)

    if os.path.exists(path_to_daospec):
        os.remove(path_to_daospec)

    if os.path.exists(path_to_r):
        os.remove(path_to_r)

    if os.path.exists(path_to_c):
        os.remove(path_to_c)


def copy_daospec_output_to_log_dir(basename, options,
                                   wavelength_start, wavelength_end,
                                   log_subdirectory):
    """
    Copies the files that DAOSPEC generates (.daospec)
    to a log directory.

    Parameters
    ----------
    basename : str
        Basename of the .fits file.
    options : dict
        Program options.
    log_subdirectory : str
        Name of the directory where the DAOSPEC output will be copied.
    wavelength_start: float
        The start of the wavelength range used by Daospec.
    wavelength_end: float
        The end of the wavelength range used by Daospec.

    """

    log_dir = os.path.join(options['daospec_log_dir'], log_subdirectory)
    fits_dir = options["fits_dir"]

    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    file_daospec = f"{basename}.daospec"

    path_to_daospec = os.path.join(fits_dir, file_daospec)
    path_to_daospec_dest = os.path.join(log_dir, file_daospec)
    copyfile(path_to_daospec, path_to_daospec_dest)


def daospec_log_subdirectory():
    """
    Returns
    -------
    str
       Subdirectory for the DAOSPEC log files
       based on the current data and time.
       Example: 2024_11_12.12_23_23
    """

    now = datetime.datetime.now()
    return f'{now.year}_{now.month}_{now.day}.{now.hour}_{now.minute}_{now.second}'


def backup_daospec_settings(options):
    """
    Makes copies of the DAOSPEC option files
    before they are overwritten by the options we want.
    After the DAOSPEC jobs finish, we will restore options from the backups.

    Parameters
    ----------
    options : dict
        Program options.
    """

    daospec_dir = daospec_working_dir(options)

    daospec_opt_path_src = os.path.join(daospec_dir, DAOSPEC_OPT_FILE_NAME)
    daospec_opt_path_dest = f'{daospec_opt_path_src}{BACKUP_SUFFIX}'
    copyfile(daospec_opt_path_src, daospec_opt_path_dest)

    laboratory_dat_path_src = os.path.join(daospec_dir, LABORATORY_DAT_FILE_NAME)
    laboratory_dat_path_dest = f'{laboratory_dat_path_src}{BACKUP_SUFFIX}'
    copyfile(laboratory_dat_path_src, laboratory_dat_path_dest)


def restore_daospec_settings(options):
    """
    Restores the backup copies of the DAOSPEC option files.
    This is called after the DAOSPEC jobs finish to restore
    the option files that DAOSPEC had in its working directory,
    that there were before Dao Assist was called.

    Parameters
    ----------
    options : dict
        Program options.
    """

    daospec_dir = daospec_working_dir(options)

    daospec_opt_path = os.path.join(daospec_dir, DAOSPEC_OPT_FILE_NAME)
    daospec_opt_path_backup = f'{daospec_opt_path}{BACKUP_SUFFIX}'
    copyfile(daospec_opt_path_backup, daospec_opt_path)

    laboratory_dat_path = os.path.join(daospec_dir, LABORATORY_DAT_FILE_NAME)
    laboratory_dat_path_backup = f'{laboratory_dat_path}{BACKUP_SUFFIX}'
    copyfile(laboratory_dat_path_backup, laboratory_dat_path)

    if os.path.exists(daospec_opt_path_backup):
        os.remove(daospec_opt_path_backup)

    if os.path.exists(laboratory_dat_path_backup):
        os.remove(laboratory_dat_path_backup)


def copy_settings_to_doaspec_dir(options):
    """
    Copies the options files specified in setting
    into daospec.opt and laboratory.dat files located in the DAOSPEC 
    working directory.

    Parameters
    ----------
    options : dict
        Program options.
    """

    daospec_dir = daospec_working_dir(options)
    daospec_opt_path_source = options["daospec_opt_path"]
    daospec_opt_path_dest = os.path.join(daospec_dir, DAOSPEC_OPT_FILE_NAME)
    laboratory_dat_path_dest = os.path.join(daospec_dir, LABORATORY_DAT_FILE_NAME)

    copyfile(daospec_opt_path_source, daospec_opt_path_dest)

    laboratory_dat_path_source = options["laboratory_dat_path"]
    print(f"laboratory_dat_path_source {laboratory_dat_path_source}")
    print(f"laboratory_dat_path_dest {laboratory_dat_path_dest}")
    copyfile(laboratory_dat_path_source, laboratory_dat_path_dest)


def files_in_dir(directory, extension):
    """
    Parameters
    ----------
    lines : list of str
        The basenames of files of given `extension` located in `directory`
    """

    basenames = []
    files_in_dir = [f for f in os.listdir(directory)]

    for filename in files_in_dir:
        file_path = os.path.join(directory, filename)

        if file_path and filename.endswith(".fits"):
            basename = os.path.splitext(filename)[0]
            basenames.append(basename)

    return basenames