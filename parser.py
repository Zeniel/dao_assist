"""
Parsing Daospec output and extracting individual values.
"""

import re
import os


def extract_results(basename, fits_dir):
    """
    Extracts results from daospec output.

    Parameters
    ----------
    basename : str
        Basename of the .fits file.
    fits_dir : str
        Directory of the .fits file.

    Returns
    -------
    list of str
       List of results for different lines from Daospec.
    """

    lines = extract_lines_of_interest(basename, fits_dir)
    output_lines = []

    for line in lines:
        ew = extract_equivalent_width(line)
        middle_value = extract_middle_value(line)
        middle_index = line.index(middle_value)
        line = line[middle_index:]
        line = line[:-1] + ew
        output_lines.append(line)

    return output_lines


def extract_fwhm(text):
    """
    Extracts the FWHM value from the Daospec output.

    Parameters
    ----------
    text : str
        Text containing the output from Daospec (text sent to stdout by Daospec).

    Returns
    -------
    float
       FWHM value or None in case of error.
    """

    match = re.search(r'\s+(\S+)\s+Final radial velocity', text)

    if match:
        return float(match.group(1))

    print("Can't extract FWHM")

    return None


def extract_equivalent_width(text):
    """
    Extracts the equivalent width from a single line returned by Daospec.

    Parameters
    ----------
    text : str
        A line corresponding to a single wavelength that is returned by the Daospec.

    Returns
    -------
    str
       Equivalent width calculated by Daospec. Returns None if error occurred.
    """

    match = re.search(r'^\S+\s+\S+\s+(\S+)\s+', text)

    if match:
        return match.group(1)

    print("Can't extract equivalent_width")

    return None


def extract_lines_of_interest(basename, fits_dir):
    """
    Read the .daospec file produced by Daospec and returns the lines that
    end with the 'c' character. These are the lines that correspond to
    the absorption line from laboratory.dat files that we want to measure.

    Parameters
    ----------
    basename : str
        Basename of the .fits file.
    fits_dir : str
        Directory of the .fits file.

    Returns
    -------
    list of str
       List of lines from Daospec output that we want to measure.
    """

    file_fits = f"{basename}.daospec"
    path_to_fits = os.path.join(fits_dir, file_fits)

    with open(path_to_fits) as f:
        lines = list(f)
        lines = [line.rstrip() for line in lines]
        lines = [line for line in lines if line[-1] == 'c']
        return lines


def extract_middle_value(text):
    """
    Extracts the text from a column that contains wavelength,
    located in the middle of the line that is returned by Daospec.

    Parameters
    ----------
    text : str
        A line corresponding to a single wavelength that is returned by the Daospec.

    Returns
    -------
    str
       The text located in the middle column
    """

    match = re.search(r'^\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)\s+', text)

    if match:
        return match.group(1)

    print("Can't extract middle value")

    return None


def extract_element_number(text):
    """
    Extracts the element number from a single line returned by Daospec.

    Parameters
    ----------
    text : str
        A line corresponding to a single wavelength that is returned by the Daospec.

    Returns
    -------
    str
       Element number (i.e. '26.1') from the Daospec output. Returns None if error occurred.
    """

    match = re.search(r'^\S+\s+(\S+)', text)

    if match:
        return float(match.group(1))

    print("Can't extract element number")

    return None


def extract_wavelength(text):
    """
    Extracts the wavelength from a single line returned by Daospec.

    Parameters
    ----------
    text : str
        A line corresponding to a single wavelength that is returned by the Daospec.

    Returns
    -------
    float
       Wavelength in Angstrom (i.e. '5752.04') from the Daospec output or None in case of error.
    """

    match = re.search(r'^(\S+)\s+', text)

    if match:
        return float(match.group(1))

    print("Can't extract wavelength")

    return None
