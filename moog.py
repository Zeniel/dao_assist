"""
Prepare output for the MOOG.
"""

import os
import functools
from parser import extract_element_number, extract_wavelength


def compare_lines(a, b):
    """
    Compares two lines from Doaspec. This function is supplied into the sorting function
    to sort the list of lines by the element name and then by the wavelength.

    Parameters
    ----------
    a, b : str
        A lines from Daospec.


    Returns
    -------
    int
        Negative number if a is greater than b
        Zero if a is the same as b
        Positive number if a is smaller than b
    """

    a_element_number = extract_element_number(a)
    b_element_number = extract_element_number(b)

    if a_element_number != b_element_number:
        return b_element_number - a_element_number

    a_wavelength = extract_wavelength(a)
    b_wavelength = extract_wavelength(b)

    return b_wavelength - a_wavelength


def sort_lines(lines):
    """
    Sorts the lines from different elements
    by the element name and then by the wavelength.

    Parameters
    ----------
    lines : list of str
        List of results for different lines from Daospec.


    Returns
    -------
    list of str
        Lines from different elements by the element name and then by the wavelength.
    """

    return sorted(
        lines,
        key=functools.cmp_to_key(compare_lines),
        reverse=True)


def is_iron_line(line):
    """
    Parameters
    ----------
    line : list of str
        Result for a single line from Daospec.

    Returns
    -------
    bool
        True if the line is for an iron element (26.0 or 26.1).
        Otherwise return False.
    """

    element_number = extract_element_number(line)

    return element_number == 26 or element_number == 26.1


def separate_lines(lines):
    """
    Saparates the lines into two lists:
        1. Lines for iron elements.
        2. Lines for non-iron elements.

    Parameters
    ----------
    lines : list of str

    List of results for different lines from Daospec.


    Returns
    -------
    (list of str, list of str)
        The tuple contains two elements:
            1. List of lines for iron elements.
            2. List of lines for non-iron elements.

        Both lists are sorted by the element name and then by the wavelength.
    """

    iron_lines = []
    non_iron_lines = []

    for line in lines:
        if is_iron_line(line):
            iron_lines.append(line)
        else:
            non_iron_lines.append(line)

    iron_lines = sort_lines(iron_lines)
    non_iron_lines = sort_lines(non_iron_lines)

    return (iron_lines, non_iron_lines)


def save_daospec_output_for_moog(basename, lines, options):
    """
    Saves the output from Daospec `lines` into files suitable for Moog,
    located in the directory in the `moog_input_dir` setting.

    Parameters
    ----------
    basename : str
        Name of the star

    lines : list of str
        List of results for different lines from Daospec.

    options : dict
        Program options.
    """

    iron_lines, non_iron_lines = separate_lines(lines)

    moog_input_dir = options["moog_input_dir"]

    if not os.path.exists(moog_input_dir):
        os.makedirs(moog_input_dir)

    # Iron lines
    # ---------------

    iron_lines_path = os.path.join(moog_input_dir, f'{basename}.fe.lines')

    with open(iron_lines_path, "w") as file:
        file.write("#\t0\t0\n")
        for line in iron_lines:
            file.write(line + "\n")

    # Non-iron lines
    # ---------------

    non_iron_lines_path = os.path.join(moog_input_dir, f'{basename}.elements.lines')

    with open(non_iron_lines_path, "w") as file:
        file.write("#\t0\t0\n")
        for line in non_iron_lines:
            file.write(line + "\n")
