import json
import os

DEAFULT_SETTING_FILE_PATH = 'settings.json'


def load_settings(settings_file_path):
    """
    Loads and returns program settings.

    Parameters
    ----------
    settings_file_path : str
        Path to the settings JSON file, or None is default path should be used.


    Returns
    ---------
    dict
        Program settings.
    """

    if settings_file_path is None:
        settings_file_path = DEAFULT_SETTING_FILE_PATH

    if not os.path.exists(settings_file_path):
        print(f'Settings file {settings_file_path} does not exist')
        exit(1)

    with open(settings_file_path) as file:
        return json.load(file)
